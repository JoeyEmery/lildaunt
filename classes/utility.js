var axios = require('axios');
var _ = require('lodash');
var logger = require('winston'); logger.level = 'debug';

var _admins = [188748084690419713, 479705115604025355, 163114518967353345];

module.exports = class Help {
	capitalize(string)
	{
		return string.charAt(0).toUpperCase() + string.substr(1);
	}

	isOrContains(message, match)
	{
		message = message.toLowerCase();
		var pieces = message.split(' '),
			result = false;

		_.each(pieces, function(piece) {
			if(piece == match)
				result = true;
		});

		return result;
	}

	updateKKs(channelID, bot)
	{
		axios.get(`http://nacho.letushost.co.uk/api/kk`)
            .then(response => {
                return bot.sendMessage({to: channelID, message: `Counted ${response.data.amount} kk's`});
            }).catch(error => {
                return bot.sendMessage({to: channelID, message: 'An error occured updating KKs.'});
            });
	}

	joinArgs(args)
	{
		if(typeof args == 'object') {
	        return args.join(' ');
	    }

	    return args;
	}

	isAdmin(id)
	{
		id = parseInt(id);

		if(_admins.indexOf(id) != -1) {
			return true;
		}

		return false;
	}
}