module.exports = class Help {
	sendHelp(args, user_id, bot)
	{
		var _help = "So, you need a little help? Below are all the commands I can do!\n\n";
	        _help += "!emoji <piratename> (Displays the Pirate's emojis)\n";
	        _help += "!crew <piratename> crew (Displays the Pirate's crew)\n";
	        _help += "!rank <piratename> rank (Displays the Pirate's rank)\n";
	        _help += "!link <piratename> rank (Returns a link to the Pirate's Yoweb)\n";
	        _help += "!setemoji <piratename> <emojis> (Sets the Pirate's emojis)\n\n";

	        _help += "!<plank> <piratename> (Planks a pirate)\n";
	        _help += "!<fuck> <piratename> (Well fuck you dude)\n";
	        //_help += "!<wakeup> (???)\n";
	        _help += "!<wegottem> (???)\n\n";

	        _help += '!pricecheck/!pc <item> (Displays a price for an item)\n';
	        _help += '!setprice/!sp <price> <item> (Sets a price for an item)\n';

	    return bot.sendMessage({to: user_id, message: _help});
	}
}