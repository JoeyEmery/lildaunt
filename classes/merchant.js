var axios = require('axios');
var _utility = require("./utility.js"); _utility = new _utility();
var _ = require('lodash');

module.exports = class Merchant {
	priceCheck(args, channel_id, bot)
	{
		axios.get(`http://nacho.letushost.co.uk/api/prices?criteria=${args}`)
			.then(response => {
				var _message = 'Found ' + response.data.length + ' matching items (most recent first)\n';

				_.each(response.data, function(item) {
					_message += '\n**' + _utility.capitalize(item.item) + '**: ' + item.price + ' (' + item.id + ')';
				});

				return bot.sendMessage({to: channel_id, message: _message});
			})
			.catch(error => {
				return bot.sendMessage({to: channel_id, message: `I couldn't find any price data matching your criteria (${args}).`});
			});
	}

	setPrice(args, channel_id, bot)
	{
		var price = args[0],
			item = _utility.joinArgs(args.splice(1));

		axios.post(`http://nacho.letushost.co.uk/api/prices`, {price: price, item: item})
			.then(response => {
				var _message = 'Your price of **' + response.data.price + '** was set for item **' + response.data.item + '**';

				return bot.sendMessage({to: channel_id, message: _message});
			})
			.catch(error => {
				return bot.sendMessage({to: channel_id, message: 'Unable to save price information, please try again.'});
			});
	}
}