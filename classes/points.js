var _utility = require("./utility.js"); _utility = new _utility();
var axios = require('axios');
var _ = require('lodash');

module.exports = class Points {
	edit(userID, args, channel_id, bot)
	{
		var user = args[0],
			amount = parseInt(args[1]);

		// If a user has been sent.
		if(user) {
			// If an amount has been sent, update.
			if(amount) {
				if(_utility.isAdmin(userID)) {
					return axios.put(`http://nacho.letushost.co.uk/api/points`, {user: user, amount: amount})
						.then(response => {
							return bot.sendMessage({to: channel_id, message: `Points for ${_utility.capitalize(response.data.user)} updated to ${response.data.amount}`});
						}).catch(error => {
							return bot.sendMessage({to: channel_id, message: 'An error occured updating points, please try again.'});
						});
				}

				return bot.sendMessage({to: channel_id, message: 'You must be an admin to edit points.'});
			}

			// No amount set, retrieve.
			return axios.get(`http://nacho.letushost.co.uk/api/points/${user}`)
				.then(response => {
					return bot.sendMessage({to: channel_id, message: `${_utility.capitalize(response.data.user)} has ${response.data.amount} point(s)`});
				}).catch(error => {
					if(error.response.status == 404) {
						return bot.sendMessage({to: channel_id, message: `${_utility.capitalize(user)} has no points set.`});
					}

					return bot.sendMessage({to: channel_id, message: 'An error occured fetching points, please try again.'});
				});
		}

		// Get all points
		axios.get(`http://nacho.letushost.co.uk/api/points`)
			.then(response => {
				if(response.data.length > 0) {
					var _message = 'Listing all points\n\n';

					_.each(response.data, function(point) {
						_message += `${_utility.capitalize(point.user)} has ${point.amount} point(s)\n`;
					});

					return bot.sendMessage({to: channel_id, message: _message});
				}

				return bot.sendMessage({to: channel_id, message: 'No points have been set yet.'});
			}).catch(error => {
				return bot.sendMessage({to: channel_id, message: 'An error occured fetching points, please try again.'});
			});
	}
}