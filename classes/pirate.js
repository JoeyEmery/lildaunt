var axios = require('axios');

module.exports = class Pirate {
	getPirate(pirate, args, channel_id, bot, success)
	{
		axios.get(`http://nacho.letushost.co.uk/api/spider/pirate/${pirate}`)
			.then(response => {
				success(pirate, args, channel_id, response);
			}).catch(error => {
				return bot.sendMessage({to: channel_id, message: 'An error occured retrieving the pirate.'});
			});
	}

	getAll(pirate, args, channel_id, bot)
	{
		this.getPirate(pirate, args, channel_id, bot, function(pirate, args, channel_id, response) {
			var _message = 'Showing all information for **' + response.data.name + '** ';

				if(response.data.emoji)
					_message += response.data.emoji;

			 	_message += '\n\n';
				_message += '**CREW**\n' + response.data.crew.name + '\n' + response.data.rank.name;
				if(response.data.crew.flag) {
					_message += '\n\n**FLAG**\n' + response.data.crew.flag.name + '\n' + response.data.flag_rank.name;
				}

				if(response.data.image)
					_message += '\n\n' + response.data.image;

            return bot.sendMessage({to: channel_id, message: _message});
		});
	}

	getRank(pirate, args, channel_id, bot)
	{
		this.getPirate(pirate, args, channel_id, bot, function(pirate, args, channel_id, response) {
            return bot.sendMessage({to: channel_id, message: response.data.rank.name});
		});
	}

	getCrew(pirate, args, channel_id, bot)
	{
		this.getPirate(pirate, args, channel_id, bot, function(pirate, args, channel_id, response) {
            return bot.sendMessage({to: channel_id, message: response.data.crew.name});
		});
	}

	getEmoji(pirate, args, channel_id, bot)
	{
		this.getPirate(pirate, args, channel_id, bot, function(pirate, args, channel_id, response) {
            return bot.sendMessage({to: channel_id, message: response.data.emoji});
		});
	}

	getLink(pirate, channel_id, bot)
	{
		return bot.sendMessage({to: channel_id, message: 'http://emerald.puzzlepirates.com/yoweb/pirate.wm?target=' + pirate});
	}

	setEmoji(pirate, args, channel_id, bot) {
		args = args.splice(1);

	    axios.post(`http://nacho.letushost.co.uk/api/pirates/${pirate}`, {emoji: joinArgs(args)})
	        .then(response => {
	            return bot.sendMessage({to: channel_id, message: pirate + "'s emojis were updated to " + joinArgs(args)});
	        }).catch(error => {
	            return bot.sendMessage({to: channel_id, message: 'An error occured updating the pirate.'});
	        });
	}

	plank(args, channel_id, user, bot) {
	    if(args[0].toLowerCase() !== 'daunt') {
	     	return bot.sendMessage({
		        to: channel_id,
		        message: '*Forces ' + joinArgs(args) + ' to walk the plank!*',
		    });   
	    }
	}
}

function joinArgs(args) {
    if(typeof args == 'object') {
        return args.join(' ');
    }

    return args;
}