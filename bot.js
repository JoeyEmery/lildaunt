var Discord = require('discord.io');
var logger = require('winston'); logger.level = 'debug';
var axios = require('axios');
var _ = require('lodash');
var auth = require('./auth.json');

// Classes
var _pirate = require("./classes/pirate.js"); _pirate = new _pirate();
var _help = require("./classes/help.js"); _help = new _help();
var _merchant = require("./classes/merchant.js"); _merchant = new _merchant();
var _utility = require("./classes/utility.js"); _utility = new _utility();
var _points = require("./classes/points.js"); _points = new _points();

var bot = new Discord.Client({
    token: auth.token,
    autorun: true
});

/*

    User IDs

    Lil Daunt       493960949305311244
    Baine           188748084690419713
    Daunt           479705115604025355
    Kate            163114518967353345

*/

bot.on('message', function (user, userID, channelID, message, evt) {

    // Don't run on LilDaunt's messages.
    if(userID == 493960949305311244) {
        return;
    }

    if(message.substring(0, 1) == '!') {
        var args = message.substring(1).split(' '),
            cmd = args[0].toLowerCase();
        
        args = args.splice(1);

        // Switch the command
        switch(cmd) {
            /* Help and command usage */
            case 'help':
                return _help.sendHelp(args, userID, bot);
            break;

            /* Pirate information */
            case 'who':
                return _pirate.getAll(args[0], args, channelID, bot);
            break;

            case 'rank':
                return _pirate.getRank(args[0], args, channelID, bot);
            break;

            case 'crew':
                return _pirate.getCrew(args[0], args, channelID, bot);
            break;

            case 'emoji':
                return _pirate.getEmoji(args[0], args, channelID, bot);
            break;

            case 'link':
                return _pirate.getLink(args[0], channelID, bot);
            break;

            case 'setemoji':
                return _pirate.setEmoji(args[0], args, channelID, bot);
            break;

            /* Merchant */
            case 'pricecheck': case 'pc':
                return _merchant.priceCheck(_utility.joinArgs(args), channelID, bot);
            break;

            /* Points */
            case 'points':
                return _points.edit(userID, args, channelID, bot);
            break;

            case 'setprice': case 'sp':
                return _merchant.setPrice(args, channelID, bot);
            break;

            /* Just for fun */
            case 'plank':
                return _pirate.plank(args, channelID, user, bot);
            break;

            case 'wegottem':
                return bot.sendMessage({to: channelID, message: 'https://youtu.be/HCikEteA6PE'});
            break;

            case 'fuck':
                if(args[0].toLowerCase() == 'kate' || args[0].toLowerCase() == 'kotoura') {
                    return bot.sendMessage({to: channelID, message: 'https://youtu.be/HCikEteA6PE'});
                }

                return bot.sendMessage({to: channelID, message: 'Yeah, fuck ' + _utility.joinArgs(args) + '!'});
            break;

            case 'users':
                logger.info(bot);
            break;
        }
    }

    // KK
    if(_utility.isOrContains(message, 'kk') && userID == 163114518967353345) {
        _utility.updateKKs(channelID, bot);
    }

    // Fortnite
    // if(_utility.isOrContains(message, 'fortnite')) {
    //     return bot.sendMessage({to: channelID, message: `Stop talking about Fortnite, Pindrop :european_castle::milky_way:`});
    // }
});